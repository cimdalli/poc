var PocModule;
(function (PocModule) {
    var DashboardController = (function () {
        function DashboardController($scope, $location, $facebook) {
            $facebook.getLoginStatus().then(function (response) {
                if (response.status !== 'connected') {
                    $location.path("/login");
                }
                else {
                    $facebook.api('/me').then(function (response) {
                        $scope.user = response;
                        console.log(response);
                    });
                }
            });
            $scope.logout = function () {
                $facebook.logout().then(function () {
                    $location.path("/login");
                });
            };
        }
        DashboardController.$inject = ['$scope', '$location', '$facebook'];
        return DashboardController;
    })();
    PocModule.DashboardController = DashboardController;
})(PocModule || (PocModule = {}));
//# sourceMappingURL=dashboardcontroller.js.map