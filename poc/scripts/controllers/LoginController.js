var PocModule;
(function (PocModule) {
    var LoginController = (function () {
        function LoginController($scope, $location, $facebook) {
            $scope.loginWithFB = function () {
                $facebook.login().then(function (response) {
                    console.log(response);
                    if (response.status === "connected") {
                        $location.path("/dashboard");
                    }
                });
            };
        }
        LoginController.$inject = ['$scope', '$location', '$facebook'];
        return LoginController;
    })();
    PocModule.LoginController = LoginController;
})(PocModule || (PocModule = {}));
//# sourceMappingURL=logincontroller.js.map