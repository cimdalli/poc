﻿module PocModule {
    export class LoginController {

        public static $inject = ['$scope', '$location', '$facebook'];

        constructor($scope, $location, $facebook) {
            $scope.loginWithFB = () => {
                $facebook.login().then(response => {
                    console.log(response);
                    if (response.status === "connected") {
                        $location.path("/dashboard");
                    }
                });
            };
        }
    }
}