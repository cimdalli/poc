﻿module PocModule {
    export class DashboardController {

        public static $inject = ['$scope', '$location', '$facebook'];

        constructor($scope, $location, $facebook) {

            $facebook.getLoginStatus().then(response => {
                if (response.status !== 'connected') {
                    $location.path("/login");
                } else {
                    $facebook.api('/me').then(response => {
                        $scope.user = response;
                        console.log(response);
                    });
                }
            });

            $scope.logout = () => {
                $facebook.logout().then(() => {
                    $location.path("/login");
                });
            };
        }
    }
}