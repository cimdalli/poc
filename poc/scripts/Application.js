/// <reference path="typings/angularjs/angular.d.ts" />
/// <reference path="controllers/maincontroller.ts" />
/// <reference path="controllers/logincontroller.ts" />
/// <reference path="controllers/dashboardcontroller.ts" />
/// <reference path="config/routerconfig.ts" />
var PocModule;
(function (PocModule) {
    angular.module("PocModule", ["ngRoute", "ngFacebook"])
        .controller("MainCtrl", PocModule.MainController)
        .controller("LoginCtrl", PocModule.LoginController)
        .controller("DashboardCtrl", PocModule.DashboardController)
        .config(PocModule.RouterConfig)
        .config(function ($facebookProvider) {
        $facebookProvider.setAppId('1091474300893112');
    })
        .run(function ($rootScope) {
        // Cut and paste the "Load the SDK" code from the facebook javascript sdk page.
        // Load the facebook SDK asynchronously
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    });
})(PocModule || (PocModule = {}));
//# sourceMappingURL=Application.js.map