var PocModule;
(function (PocModule) {
    var RouterConfig = (function () {
        function RouterConfig($routeProvider, $locationProvider) {
            $routeProvider
                .when("/login", {
                controller: "LoginCtrl",
                templateUrl: "login.html"
            })
                .when("/dashboard", {
                controller: "DashboardCtrl",
                templateUrl: "dashboard.html"
            });
            $routeProvider.otherwise({ redirectTo: "/login" });
        }
        RouterConfig.$inject = ['$routeProvider', '$locationProvider'];
        return RouterConfig;
    })();
    PocModule.RouterConfig = RouterConfig;
})(PocModule || (PocModule = {}));
//# sourceMappingURL=routerconfig.js.map