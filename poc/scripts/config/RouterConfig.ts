﻿module PocModule {
    export class RouterConfig {

        public static $inject = ['$routeProvider', '$locationProvider'];

        constructor($routeProvider, $locationProvider) {
            $routeProvider
                .when("/login", {
                    controller: "LoginCtrl",
                    templateUrl: "login.html"
                })
                .when("/dashboard", {
                    controller: "DashboardCtrl",
                    templateUrl: "dashboard.html"
                });

            $routeProvider.otherwise({ redirectTo: "/login" });
        }
    }
}