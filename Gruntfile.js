/*jslint node: true */
"use strict";

module.exports = function (grunt) {
    grunt.initConfig({


        watch: {
            options: {
                livereload: true
            },
            grunt: {
                files: ['Gruntfile.js'],
                tasks: ['build']
            },
            ts: {
                files: ['poc/scripts/**/*.ts'],
                tasks: ['ts']
            }
        },

        connect: {
            server: {
                options: {
                    port: 8999,
                    base: {
                        path: "poc",
                        options: {
                            index: "index.html"
                        }
                    }
                }
            }
        },

        copy: {
            admin: {
                expand: true, cwd: "poc/admin-lte/", src: ["**"], dest: "poc/dist/"
            },
            default: {
                files: [
                    { src: "bower_components/bootstrap/dist/css/bootstrap.min.css", dest: "poc/dist/css/bootstrap.min.css" },
                    { src: "bower_components/bootstrap/dist/js/bootstrap.min.js", dest: "poc/dist/js/bootstrap.min.js" },
                    { src: "bower_components/jquery/dist/jQuery.min.js", dest: "poc/dist/js/jQuery.min.js" },
                    { src: "bower_components/angular/angular.min.js", dest: "poc/dist/js/angular.min.js" },
                    { src: "bower_components/angular-route/angular-route.min.js", dest: "poc/dist/js/angular-route.min.js" },
                    { src: "bower_components/ng-facebook/ngFacebook.js", dest: "poc/dist/js/ngFacebook.js" }
                ]
            }
        },

        clean: {
            default: {
                src: "poc/dist"
            }
        },

        ts: {
            default: {
                src: ["poc/scripts/**/*.ts"],
                out: "poc/dist/js/poc.js",
                options: {
                    sourceMap: false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks("grunt-ts");


    grunt.registerTask('build', ['clean', 'copy', 'ts']);
    grunt.registerTask('dev', ['build', 'connect', 'watch']);
    grunt.registerTask('default', ['build']);

};